var controllers = {};

controllers.DoctorController = function($rootScope, $location, $localstorage, $ionicLoading, $ionicPopup,  $http, $scope) {
  $scope.currentPage = 1;
  $scope.totalPages = 5;
  $scope.totalNumberofDoctors =  null;

  $scope.clearSearch= function(){
    $scope.doctorText = "";
  }

  $scope.getDoctors = function(page){
    $ionicLoading.show({
      template: 'Getting list of Doctors...'
    });

    param = $scope.doctorText? "/api/search/" + "?q=" + $scope.doctorText : "/api/search/?";

    if(page){
      if ((page=="prev") && ($scope.currentPage >1)){
        param = param + '&page='+ ($scope.currentPage - 1);
      }else if ((page=="next") && ($scope.currentPage < $scope.totalPages)){
        param = param + '&page='+ ($scope.currentPage + 1)
      }
    }
    $http.get($rootScope.apiBaseAddress + param ).success(function(data){
      $ionicLoading.hide();

      if ((page ==="next") && ($scope.totalPages === $scope.currentPage))
        return;
      $scope.currentListofDoctors = {};

      try{
        cordova.plugins.Keyboard.close();
      }catch(e){}

      // $http.get( 'http://demo.nlocate.com:8080' + param ).success(function(data){
      $scope.currentListofDoctors = data.items;
      $scope.totalPages = data.paging.total;
      $scope.currentPage = data.paging.current;
      $scope.totalNumberofDoctors = data.paging.doc_count;

      if ($scope.currentListofDoctors.length == 0){
        $scope.currentListofDoctors = [{ errorText: "No Results to Show"}]
      }

    }).error(function(data, s){
      $ionicLoading.hide();
      $rootScope.errorPopup(true);
    });
  };

  $scope.claim = function(doctorId){
    $scope.claimMessage = {};
    $ionicPopup.show({
      title: 'Claim',
      scope: $scope,
      template: "Include a message to Administrator" +
        "<textarea style='height:100px; padding: 5px 10px;' ng-model='claimMessage.text' type='text'/></textarea>",
        buttons: [
          {text: 'cancel'},
          {
            text: '<b>Submit</b>',
            type: 'button-calm',
            onTap: function(e){
              if ($scope.claimMessage.text){

                $ionicLoading.show();
                var postMessage= {
                  message : $scope.claimMessage.text 
                };
                $http.get($rootScope.apiBaseAddress + '/token/')
                .success(function(data){
                  $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;
                  $http.post( $rootScope.apiBaseAddress + '/claim/doctor/' + doctorId +'/' , postMessage)
                  .success(function(data, status_, headers, config){
                    $ionicLoading.hide();

                    if (data.ok == true){
                      $rootScope.alertPopup('Success', 'Your Claim was posted');
                      $rootScope.isClaimed = true;
                      $localstorage.set('isClaimed', true);
                      $location.path('userCases');
                    }else{
                      $rootScope.alertPopup('Error', 'You have already filed a claim.'+
                                            'Please withdraw  your last claim to make a new one');
                    }
                  })
                  .error(function(){
                    $ionicLoading.hide();
                  })
                })
                //here
              }
            }
          }
        ]
    }).then(function(){
    });
  }

  $scope.placeCall = function(number){
    window.open("tel:" + number, '_system', 'location=no');
  }

  $scope.getDetails = function (id){

    $rootScope.doctorDetailId = id;
    $location.path('doctorDetails', true);
  }
  $scope.getDoctors();
}

controllers.FeedController = function($rootScope, $location, $localstorage, $ionicLoading, $ionicPopup,  $http, $scope) {
  // $scope.data =  [
  // 	{
  // 		'title': 'title',
  // 		'published_date' : Date(),
  // 	}
  // ]
  $ionicLoading.show();
  $http.get ( $rootScope.apiBaseAddress + '/api/newsfeed/feed/')
  .success(function(data){
    $scope.data = data.items;
    $ionicLoading.hide();
    for ( i in $scope.data ){
      $scope.data[i].published_date = moment($scope.data[i].published_date).fromNow();
    }
  })
  .error(function(){
    $ionicLoading.hide();
    alert('Error!');
  })
}

controllers.DoctorDetailsController = function($rootScope, $location, $localstorage, $ionicLoading, $ionicPopup,  $http, $scope) {
  var id = $rootScope.doctorDetailId;
  $scope.doctor = {};
  $scope.fetchDoctorDetails = function(){
    $ionicLoading.show();
    $http.get($rootScope.apiBaseAddress +'/api/doctor/'+ id +'/' )
    .success(function(res){
      $ionicLoading.hide();

      if (res.data)
        $scope.doctor = res.data;
      if ($scope.doctor.photo)
        $scope.doctor.photo =  $rootScope.apiBaseAddress + "/static/profile/400x400/" + $scope.doctor.photo;

    })
    .error(function(res){
      $ionicLoading.hide();
    })
  }
  $scope.fetchDoctorDetails();
}

controllers.PostController = function($rootScope, $timeout, $ionicLoading,   $stateParams, $window, $state,  $http, $location, $scope) {
  $scope.case = {};
  $scope.hasErrors = false;
  $scope.errors = [];
  $scope.submit = function(){
    postMessage= {
      title: $scope.case.title,
      message: $scope.case.message,
    }
    if (!$scope.case.title || !$scope.case.message){
      $scope.errors.push('All fields are requred!');
    }
    if ($scope.errors.length > 0){
      $scope.hasErrors = true;
    }else{
      $scope.hasErrors = false;
    }
    // if ($scope.errors)


    $http.get($rootScope.apiBaseAddress + '/token/')
    .success(function(data){
      $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;

      $http.post($rootScope.apiBaseAddress+ '/case/add/', postMessage)
      .success(function(data, status_, headers, config){
        $rootScope.caseId = data.case_id;
        $ionicLoading.show({
          template: 'loading..'
        })
        $timeout(function(){
          $state.go('userCases', {}, {reload: true, location: 'replace'});
        }, 1000);
        // location.reload();
        // $state.transitionTo('userCases', $stateParams, { reload: true, inherit: true, notify: true });
      })
    });
  }
}

controllers.JournalController = function($rootScope, $ionicLoading, $ionicPopup,  $scope, $http) {
  $scope.items = [];

  $scope.getJournals = function (query){
    $ionicLoading.show({
      template: "Getting Journals.."
    });

    var searchTerm = query ? '/journal/search/?q=' + query + '/' : '/journal/search/';

    $http.get($rootScope.apiBaseAddress + '/token/')
    .success(function(data){
      $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;
      $http.get( $rootScope.apiBaseAddress + searchTerm )
      .success(function (response){
        $ionicLoading.hide();
        if (response.items.length == 0){
          var alertPopup = $ionicPopup.alert({
            title: 'Sorry',
            template: 'No results to show for this query'
          });
          alertPopup();
          return;
        }
        $scope.journals = response.items;
      })
    })
    .error(function(error){
      $ionicLoading.hide();
      var alertPopup = $ionicPopup.alert({
        title: 'Sorry',
        template: 'We ran into some problems. '
      });
      alertPopup();
    })
  }

  $scope.viewJournal = function ( j ){

    // window.open($rootScope.apiBaseAddress + '/journal/view/' + j.id + "/", '_blank', 'location=yes' );
    var confirmPopup = $ionicPopup.confirm({
      title: 'View Journal',
      template: "This will open an external link",
      okText: 'OK',
      cancelText: 'Stay Here'
    });
    confirmPopup.then(function(res) {
      if(res) {
        window.open($rootScope.apiBaseAddress + '/journal/view/' + j.id + "/", '_blank', 'location=yes,enableviewportscale=yes' );
      }
    })
  }

  $scope.getJournals();
};

controllers.LandingController = function($state, $http,  $ionicPopup, $localstorage,  $scope, $rootScope){
  $scope.signOut = function(){
    var confirmPopup = $ionicPopup.confirm({
      title: 'Sign Out',
      template: "Are you sure you want to sign out?",
      okText: 'Yes ',
      cancelText: 'No'
    });
    confirmPopup.then(function(res) {
      if(res) {

        $http.get($rootScope.apiBaseAddress + '/logout/' )
        .success(function(data){

          $rootScope.isDoctor = false;
          $rootScope.isLoggedIn = false;
          $localstorage.set('loggedIn', '');
          $localstorage.setObject('userInfo', {});
        })
        .error(function(d){


          $ionicPopup.alert({
            title: 'Sorry',
            template: "Couldn't sign out. Are you connected to the internet?",
          }).then(function(){

          })
        })
      } else {

      }
    });
  }
}

controllers.ReferencesController = function($state, $stateParams, $window, $localstorage ,$ionicModal, $http, $scope, $location, $rootScope, $timeout, $ionicLoading, $ionicPopup){
  $scope.bmidata = {
    feet: 5,
    inches: 4,
    kgs: 60 
  };
  $scope.pg = {};
  $scope.errors = [];
  $scope.currentData = "";
  $scope.pgdate = {};
  $scope.dataBeforeFilter = null;

  var NepaliMonths = [
    'Baisakh',
    'Jestha',
    'Ashadh',
    'Shrawan',
    'Bhadra',
    'Asoj',
    'Kartik',
    'Mangsir',
    'Poush',
    'Magh',
    'Falgun',
    'Chaitra'
  ]

  var populate = function(filter){
    var output =[];

    for (i = 0; i < filter.length; i++){
      if (/\w+,,$/.exec(filter[i].trim())){
        output.push("<div class='row gray_highlight'><div class='col'>" + filter[i].slice(0,-1).split(",").join("</div><div class='col'>") + "</div></div>");
      }else{
        output.push("<div class='row'><div class='col'>" + filter[i].slice(0,-1).split(",").join("</div><div class='col'>") + "</div></div>");
      }
    }

    output = "<ion-scroll><div id='table_here'>" + output.join("") + "</div></ion-scroll>";
    angular.element(document.getElementById('table_here')).remove()
    $timeout(function(){
      angular.element(document.getElementById('table_container')).append(output);
    },1000);
  }

  $scope.bmiResult = {
    bmi: 0,
    category: "",
  };

  $scope.calculateBMI = function(){
    $scope.hasErrors = false;

    // basic validation
    Object.keys($scope.bmidata).forEach(function(i){
      item = $scope.bmidata[i];
      if (isNaN(item) || (item < 0)) {
        $scope.hasErrors = true;
        $scope.error = "Please use positive numbers only" ;
      }
    })

    // convert to metric
    feet_cms = $scope.bmidata.feet * 30.48;
    inches_cms = $scope.bmidata.inches * 2.54; 
    height_meters = (feet_cms + inches_cms) / 100;

    $scope.bmiResult.bmi = ($scope.bmidata.kgs / (height_meters * height_meters)).toFixed(2); 

    // categorize
    var bmi = $scope.bmiResult.bmi ;
    var assessment = "";

    switch (true) {
      case (bmi < 16):
        assessment = "Severely Thin";
        break;
      case (bmi < 17):
        assessment = "Moderately Thin";
        break;
      case (bmi < 18.5):
        assessment = "Mildly Thin";
        break;
      case (bmi < 25):
        assessment = "Normal";
        break;
      case (bmi < 30):
        assessment = "Overweight";
        break;
      case (bmi < 35):
        assessment = "Class I Obese";
        break;
      case (bmi < 40):
        assessment = "Class II Obese";
        break;
      case (bmi > 40):
        assessment = "Class III Obese";
        break;
    }
    $scope.bmiResult.assessment = assessment;

  }

  function isValid(pgdate){
    if ( pgdate.date || pgdate.month || pgdate.year ){
      if (pgdate.day <= 0 || pgdate.day > 32){
        alert( 'Please enter valid date!' )
        return false;
      }
      if (pgdate.month <= 0 || pgdate.month > 12){
        alert( 'Please enter valid month!' )
        return false;
      }
      if (pgdate.year < 2070 || pgdate.year > 2080){
        alert( 'Please enter valid year!' )
        return false;
      }
    }else{
      alert('please enter date');
      return false
    }
  }

  $scope.pg_calculate = function( lastDate ){
    if ( isValid($scope.pgdate) ){
      return;
    }

    // convert to ad
    var lastDate_ad = Calendar($scope.pgdate.year, $scope.pgdate.month, $scope.pgdate.date).convertToAD().toADString();

    // add 9 months and 7 days
    var delta = moment(lastDate_ad).add(9, 'months').add(7, 'days').format("DD MM YYYY").split(' ');
    delta = delta.map(function(item){ return parseInt(item)});

    // convert back to BS
    var inNepali = Calendar(delta[2], delta[1], delta[0]).convertToBS().toBSString().split('-').reverse();

    var nepaliDateString = inNepali[0] + ' ' +  NepaliMonths[parseInt(inNepali[1]) - 1] + ', ' + inNepali[2];
    $scope.pg.expected_date = nepaliDateString;
  }

  $scope.fillData = function(fileName, title){
    var title_element = '<div id="title_here"><div style="color:white; font-size: 20px;margin-left: 20px;">'+ title +'</div></div>'; 

    angular.element(document.getElementById('title_here')).remove();
    $timeout(function(){
      angular.element(document.getElementById('title_container')).append(title_element);
    }, 50);

    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", 'data/'+ fileName +'.csv', true);
    rawFile.onreadystatechange = function () {
      if(rawFile.readyState === 4) {
        if(rawFile.status === 200 || rawFile.status == 0) {
          var allText = rawFile.responseText;
          var data = allText;
          $scope.currentData = allText;

          lines = data.split("\n")
          var output = [],
            regex = /,,$/i,
              i;


              for (i = 0; i < lines.length; i++){
                currentLine = lines[i];
                if (currentLine.trim().search(/\w+,,$/) !== -1){
                  output.push(
                    "<div class='row gray_highlight'>"
                    + lines[i].slice(0,-1).split(",").join("")
                    + "</div>"
                    // "<div class='row gray_highlight'><div class='col'>"
                    // + lines[i].slice(0,-1).split(",").join("</div><div class='col'>")
                    // + "</div></div>"
                  );
                }
                else{
                  output.push(
                    "<div class='row'><div class='col'>" 
                    + lines[i].slice(0,-1).split(",").join("</div><div class='col'>") 
                    + "</div></div>"
                  );
                }

              }
              output = "<ion-scroll><div id='table_here'>" + output.join("") + "</div></ion-scroll>";
              angular.element(document.getElementById('table_here')).remove()
              $timeout(function(){
                angular.element(document.getElementById('table_container')).append(output);
              },500);

        }
      }
    }
    rawFile.send(null);
  }

  $scope.filterList = function(searchTerm){
    if (!$scope.dataBeforeFilter) $scope.dataBeforeFilter = lines;

    var filter = [];
    if (!searchTerm){
      populate($scope.dataBeforeFilter || lines);
      return;
    }

    // filters the csv lines containing the searchTerm 
    // and onwards
    var lines_ = [].concat(lines);
    lines_.forEach(function(line){
      if (line.toLowerCase().search(searchTerm.toLowerCase()) !== -1){
        afterList = lines_.splice(lines_.indexOf(line), lines_.length - 1);
        // filter = afterList;
      }
    })
    filter.push(afterList[0]);
    for ( var idx = 1; idx < afterList.length; idx++){
      if (afterList[idx].trim().search(/\w*,,$/) !== -1)
        break

      filter.push(afterList[idx])
    }
    populate(filter);
  }
}

controllers.UserCaseController = function($state, $stateParams, $window, $localstorage ,$ionicModal, $http, $scope, Upload,  $location, $rootScope, $timeout, $ionicLoading, $ionicPopup){
  $scope.cases = [];
  $scope.claimer = {};
  $scope.verification_pending = false;
  $scope.files = { 'nothin here' : '' };

  $scope.claimStatus = {};
  $scope.userDetails = {};

  // from here
  $scope.$watch('files', function () {
    $scope.upload($scope.files);
  });

  $scope.upload = function (files) {
    var address = $rootScope.apiBaseAddress + "/case/comment/" + $rootScope.caseId + "/";

    $http.get($rootScope.apiBaseAddress + '/token/')
    .success(function(data){
      var token = data.csrf_token;


      if (files && files.length) {
        for (var i = 0; i < files.length; i++) {
          var file = files[i];
          //here
          confirmPopup = $ionicPopup.confirm({
            title: 'Upload Photo',
            template: 'Are you sure you want to upload this photo?'
          }).then(function(res){
            if (res){
              Upload.upload({
                url: $rootScope.apiBaseAddress +'/profile/picture/',
                headers: {'X-CSRFToken': token },
                fields: {'username': $scope.username},
                file: file
              }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);

              }).success(function (data, status, headers, config) {

                $state.go($state.current, {}, {reload: true});
              }).error(function (data, status, headers, config) {

                confirmPopup = $ionicPopup.alert({
                  title: 'Failed',
                  template: 'Sorry, the photo failed to upload'
                });
              })
            }
          }); 
          //here

        }
      }
    })

  }; 
  // to here

  $rootScope.viewDetail = function(thisCase){
    $location.path('caseDetail');
    console.log('thiscase', thisCase)
    if (thisCase){
      $rootScope.caseId = thisCase.id;
    }

    var address = $rootScope.apiBaseAddress + "/case/view/" + $rootScope.caseId + "/?"+ Math.random();
    $http
    .get(address)
    .success(function(data, status_, headers, config){
      console.log(data.case)
      $rootScope.thisCase = data.case;
    });
  }

  $scope.reply= function (text){
    postMessage = { comment: text }
    var address = $rootScope.apiBaseAddress + "/case/comment/" + $rootScope.caseId + "/";
    $http.get($rootScope.apiBaseAddress + '/token/')
    .success(function(data){
      $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;
      $http
      .post(address, postMessage)
      .success(function(data, status_, headers, config){
        if (data.ok == true){
          // $location.path('caseDetail');
          $rootScope.viewDetail();
          // location.reload();
          $scope.closeModal();
        }
      });
    });
  }

  $scope.deleteComment = function(commentId){


    var confirmPopup = $ionicPopup.confirm({
      title: 'Delete Comment',
      template: "Are you sure you want to delete this comment? ",
      okText: 'Yes ',
      cancelText: 'No'
    });
    confirmPopup.then(function(res) {
      if(res) {
        $http
        .get($rootScope.apiBaseAddress + "/case/comment/"+ $rootScope.caseId +"/"+ commentId+"/delete/"  )
        .success(function(data){

          // $location.path('case')
          $rootScope.viewDetail()
          // location.reload();
        })
      } else {

      }
    });

  }

  $scope.deleteCase = function(caseID){


    var confirmPopup = $ionicPopup.confirm({
      title: 'Delete Case',
      template: "Are you sure you want to delete this case? ",
      okText: 'Yes ',
      cancelText: 'No'
    });
    confirmPopup.then(function(res) {
      if(res) {

        $http
        .get($rootScope.apiBaseAddress + "/case/delete/"+ caseID +"/"  )
        .success(function(data){

          $ionicLoading.show({
            template: 'loading..'
          })
          $timeout(function(){
            $scope.fetch();
          }, 1000);
        })
      } else {

      }
    });


  } ; //write  a function here;

  $ionicModal.fromTemplateUrl('reply-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function() {
    $scope.modal.show();
  };

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });

  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

  $scope.checkStatus = function(){
    $http.get($rootScope.apiBaseAddress + "/claim/status/")
    .success(function(d){
      $scope.claimStatus = d.data;
      console.log(d)

      if (d.data && d.data.has_claim && !d.data.claim_approved){
        $localstorage.set('isClaimed', true);
        $rootScope.isClaimed = true;
        $scope.verification_pending = true;
      }else{
        $scope.verification_pending = false;
      };

      if (d.data && d.data.claim_approved){
        $scope.claimable = false;
        $scope.verification_pending = false;
      }

    });
  }

  $scope.fetch = function(searchTerm){
    var USERCASEADDRESS = $rootScope.apiBaseAddress + "/case/user/";

    if (searchTerm){
      USERCASEADDRESS  += "?q=" + searchTerm;
    }

    $scope.checkStatus();
    $scope.fetchProfile();

    $http.get (USERCASEADDRESS)
    .success(function(data, status_, headers, config){
      $ionicLoading.hide();
      console.log('bleh!', data.items)
      $scope.cases = data.items;
    })
    .error(function(e){
      var case_ = {
        title: "Fetching Failed"
      }
      $scope.cases.push(case_);
    });

    $http.get($rootScope.apiBaseAddress + "/claim/user_claimed_entity/" )
    .success(function(data){
      if (data.ok == true){
        $localstorage.set('isClaimed', true);
        $rootScope.isClaimed = true;
        $localstorage.setObject('userinfo', data.item);
        $scope.claimer = data.item;
      } else {
        $scope.claimStatus = null
      }
    })

  } //end of fetch

  $scope.withdraw = function(id){
    $ionicLoading.show();
    $http.get($rootScope.apiBaseAddress + '/claim/withdraw/')
    .success(function(d){
      $ionicLoading.hide();

      $scope.checkStatus();
    })
    .error(function(){
      $ionicLoading.hide();
    })
  }

  $scope.fetchProfile = function(){
    $http.get($rootScope.apiBaseAddress + '/profile/view/')
    .success(function(d){
      if (d.ok){
        $scope.userDetails = d.item;

        if ($scope.userDetails.photo ) {
          $scope.userDetails.photo = $rootScope.apiBaseAddress+ "/static/profile/400x400/"  + $scope.userDetails.photo ;
        }
      }
    })
  }

  $scope.fetch();
}

controllers.EditProfileController = function($state, $localstorage, $http, $scope, $rootScope, $location, $ionicLoading, $ionicPopup ){
  $scope.fetchProfile = function(){
    $ionicLoading.show({
      template: 'Fetching profile data'
    });
    $http.get($rootScope.apiBaseAddress + '/profile/view/')
    .success(function(d){
      $ionicLoading.hide();

      if (d.ok){
        Object.keys(d.item).forEach(function(i){
          if ((typeof(d.item[i])=='string') && d.item[i].match(/^\d+$/)){
            d.item[i]  =  +d.item[i];
          }
        });


        $scope.user = d.item;
      }
    })
    .error(function(){
      $ionicLoading.hide();
    })
  }

  $scope.submitForm = function(){
    $ionicLoading.show({
      template: 'submitting form...'
    });

    $http.get($rootScope.apiBaseAddress + '/token/')
    .success(function(data){
      $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;

      $http
      .post($rootScope.apiBaseAddress + '/profile/edit/' , $scope.user)
      .success(function(data, status_, headers, config){
        $ionicLoading.hide();

        if (data.ok){
          alert('Successfully Posted');
          location.path('userDetails');
        }else{
          alert('Please check all the fields and try again!');
        }
      })
      .error(function(){
        $ionicLoading.hide();
        alert('there was a problem submitting');
      })
    })
  }

  $scope.fetchProfile();
}

controllers.CaseController = function($state, $ionicLoading, $localstorage ,$ionicModal, $http, $scope, $location, $timeout, $ionicPopup, $rootScope){
  $scope.cases = [];

  $scope.reply= function (text){

    if (!text){
      var confirmPopup = $ionicPopup.alert({
        title: '',
        template: "Please enter some text."
      });
      confirmPopup();
    }
    postMessage={
      comment: text
    }
    var address = $rootScope.apiBaseAddress + "/case/comment/" + $rootScope.caseId + "/";
    $http.get($rootScope.apiBaseAddress + '/token/')
    .success(function(data){
      $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;
      $http
      .post(address, postMessage)
      .success(function(data, status_, headers, config){

        if (data.ok == true){
          // $location.path('caseDetail');
          $rootScope.viewDetail();
          // location.reload();
          $scope.closeModal();
        }else{
        }
      });


    });
  }

  $scope.vote =  function( commentId, thisCase, index ){

    $http.get($rootScope.apiBaseAddress + '/token/')
    .success(function(data){
      $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;
      $http
      .post($rootScope.apiBaseAddress + "/case/comment/" + $rootScope.caseId +"/"+ commentId+"/vote/", {} )
      .success(function(data){

        document.getElementById(''+index).style.display = 'none';
        $scope.hideLike = true;
        if ( data.message === "Not Logged In"){
          alert('You need to login to be able to do that.') ;
          $location.path('login');
        }
      })
      .error(function(d){

      })
    })
  }

  // not implemented
  $scope.unVote =  function( commentId ){

    $http
    .post($rootScope.apiBaseAddress + "/case/comment/"+ $rootScope.caseId +"/"+ commentId+"/un_vote/", {} )
    .success(function(data){

      if ( data.message === "Not Logged In"){
        alert('You need to login to be able to do that.') ;
        $location.path('login');
      }

      // $location.path('case')
      // location.reload();
    })
    .error(function(d){

    })
  }

  $scope.deleteComment = function( commentId ){


    if (! $rootScope.isLoggedIn){
      var confirmPopup = $ionicPopup.confirm({
        title: 'Not Logged In',
        template: "Sorry. You need to be logged in to be able to do that.",
        okText: 'Log In',
        cancelText: 'Stay Here'
      });
      confirmPopup.then(function(res){
        if (res){
          $state.go('login');
        }
      })
    }else{

      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete Comment',
        template: "Are you sure you want to delete this comment? ",
        okText: 'Yes ',
        cancelText: 'No'
      });
      confirmPopup.then(function(res) {
        if(res) {
          $http
          .get($rootScope.apiBaseAddress + "/case/comment/"+ $rootScope.caseId +"/"+ commentId+"/delete/"  )
          .success(function(data){

            // $location.path('case')
            $rootScope.viewDetail()
            // location.reload();
          })
        } else {

        }
      });
    }

  }

  $ionicModal.fromTemplateUrl('reply-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function() {
    $scope.modal.show();
  };

  $scope.closeModal = function() {
    $scope.modal.hide();
  };

  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });

  $scope.$on('modal.hidden', function() {
    // Execute action
  });

  $scope.$on('modal.removed', function() {
    // Execute action
  });

  $scope.fetch = function(searchTerm){

    $ionicLoading.show({
      template: 'Getting Cases..'
    });
    var address = $rootScope.apiBaseAddress + "/case/search/";

    if (searchTerm){
      var address = address + "?q=" + searchTerm;
    }

    $http
    .get(address)
    .success(function(data, status_, headers, config){
      $scope.cases = data.items;
      $ionicLoading.hide();
    })
    .error(function(s){
      $ionicLoading.hide()
      $timeout(function() {
        $rootScope.errorPopup()
      }, 0)
    });
  }

  $scope.fetch();
}

controllers.LoginController = function($rootScope, $localstorage, $location, $q, $ionicPopup, $ionicModal,  $scope, $http) {
  $scope.hasErrors = false;
  $scope.errors = [];

  $scope.forgotPassword = function (){
    window.open('http://nma.nlocate.com/password_reset', '_system');
  }

  validate = function(){
    usernamePattern = /^[a-zA-Z0-9@_\+\-]*$/g;
    mobileNoPattern = /^9[87][0-9]{8}$/g;
    emailPattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if ( $scope.username == undefined || $scope.username.length <= 0 ){
      $scope.errors.push("username cannot be empty");
    }else{
      if ($scope.username.length >= 30){
        $scope.errors.push("username too long. Please use 30 characters or less!");
      }
      if ($scope.username.length < 5){
        $scope.errors.push("username too short.");
      }
      if (!usernamePattern.test($scope.username)){
        $scope.errors.push("username contains invalid characters.");
      }
    }

    if($scope.mobile == undefined || $scope.mobile.length <=0 ){
      $scope.errors.push('mobile number is requred');
    }else{
      if (!mobileNoPattern.test($scope.mobile)){
        $scope.errors.push("Please enter a valid mobile number.");
      }
    }

    if($scope.email == undefined || $scope.email.length <=0 ){
      $scope.errors.push('email cannot be empty.');
    }else{
      if (!emailPattern.test($scope.email)){
        $scope.errors.push('Please enter a valid email address.');
      }
    }

    if ( $scope.password == undefined ){
      $scope.errors.push("password cannot be empty.");
    }

    if($scope.password){
      if ( $scope.password != $scope.password2 ){
        $scope.errors.push("passwords don't match.");
      }
    }

    if ($scope.errors.length > 0){
      $scope.hasErrors = true;
    }else{
      $scope.hasErrors = false;
    }
  }

  $scope.login  = function(){
    $scope.errors = [];
    if ($scope.username == undefined || $scope.password == undefined){
      $scope.errors.push("All fields are required!");
      $scope.hasErrors = true;
      return;
      // return;
    }
    postMessage = {
      username: $scope.username || "n" ,
      password: $scope.password || "p"
    }


    $http.get($rootScope.apiBaseAddress + '/token/')
    .success(function(data){
      $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;

      $http
      .post($rootScope.apiBaseAddress + "/login/", postMessage)
      .success(function(data, status_, headers, config){

        if (data.ok == false){
          var alertPopup = $ionicPopup.alert({
            title: 'Incorrect Credentials',
            template: "Username and password don't match. <br/>Please enter a valid phone number or email and the correct password."
          });
          alertPopup.then(function(res) {

            $scope.password = "";
          });
        }else if(data.ok == true){
          $localstorage.set ('loggedIn', 'true');
          $rootScope.isLoggedIn = true;
          $localstorage.setObject('userInfo', {
            name: $scope.name,
            username: $scope.username,
          });
          $rootScope.isDoctor = true;
          $location.path('landing');


        }
      });
    })
    ;

    if ($scope.errors.length > 0){
      $scope.hasErrors = true;
    }else{
      $scope.hasErrors = false;
    }
  }

  $scope.signUp  = function(){

    $scope.errors = [];

    validate();
    if (!($scope.errors.length >0)){
      postMessage = {
        mobile: $scope.mobile,
        full_name: $scope.name,
        email: $scope.email,
        username: $scope.username,
        username: $scope.username,
        password1: $scope.password,
        password2: $scope.password2
      }


      $http
      .get($rootScope.apiBaseAddress + '/token/')
      .success(function(data){
        $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;
        $http
        .post($rootScope.apiBaseAddress + "/signup/", postMessage)
        .success(function(data, status_, headers, config){
          if (data.ok == false){
            $scope.hasErrors = true;

            if (data.status = 'SIGNUP_ERROR'){
              $rootScope.setModalDisplayer(true);
            }
            angular.forEach(data.errors, function(value, key){
              $scope.errors.push(value[0]);
            })
          }else if(data.ok == true){
            $rootScope.toggleModalFlag = true;
            $rootScope.setModalDisplayer(true);
            $scope.openModal();
          }
        });
      });
      if ($scope.errors.length > 0){
        $scope.hasErrors = true;
      }else{
        $scope.hasErrors = false;
      }
    }
  }

  $scope.verifySubmit = function(verification_code, resend){
    $scope.verification_code = verification_code;

    $scope.verify(resend);
  }

  $scope.verify = function(resend){

    postMessage = {
      activation_key: $scope.verification_code,
      mobile: $scope.mobile
    }

    if(resend){

      $http.get($rootScope.apiBaseAddress + '/token/')
      .success(function(data){
        $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;
        $http
        .post($rootScope.apiBaseAddress + "/resend_verification/", {'mobile' : postMessage.mobile})
        .success(function(data, status_, headers, config){
          if (data.ok == true){
            var alertPopup = $ionicPopup.alert({
              title: '',
              template: 'Verification Code resent. Please check your SMS.'
            });
            alertPopup.then(function(res) {

            });
          }else{
            var alertPopup = $ionicPopup.alert({
              title: 'Sorry',
              template: 'You might be doing that too often. Please try in 24 hours, or contact support.'
            });
            alertPopup.then(function(res) {

            });
          }
        });
      });
    }else{

      $http.get($rootScope.apiBaseAddress + '/token/')
      .success(function(data){
        $http.defaults.headers.post['X-CSRFToken'] = data.csrf_token;
        $http
        .post($rootScope.apiBaseAddress + "/verify/", postMessage)
        .success(function(data, status_, headers, config){
          if(data.ok == true ){
            $rootScope.setModalDisplayer (false);
            var alertPopup = $ionicPopup.alert({
              title: '',
              template: 'Verification Successful! <br/>Please log in.'
            });
            alertPopup.then(function(res) {
              $scope.closeModal();
              $location.path('login');
            });
          }else if (data.ok == false){
            var alertPopup = $ionicPopup.alert({
              title: '',
              template: 'There was a problem with verification. Please check your code!'
            });
            alertPopup.then(function(res) {
            });
          }
        });
      });
    }
  }

  $ionicModal.fromTemplateUrl('my-modal.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function() {
    $scope.modal.show();
  };

  $scope.closeModal = function() {
    $scope.modal.hide();
  };
  $scope.$on('$destroy', function() {
    if ($scope.modal) $scope.modal.remove();
  });
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  $scope.$on('modal.removed', function() {
    // Execute action
  });
}

nmaApp.controller(controllers);
