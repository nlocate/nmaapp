nmaApp.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('landing', {
    url: "/",

    views: {
      'bodyContent' : {
        templateUrl: "templates/landing.html",
        controller: "LandingController"
      }
    }
  })

  .state('references', {
    url: "/references",

    views: {
      'bodyContent' : {
        templateUrl: "templates/references.html",
        controller: "ReferencesController"
      }
    }
  })

  .state('feed', {
    url: "/feed",

    views: {
      'bodyContent' : {
        templateUrl: "templates/feed.html",
        controller: "FeedController"
      }
    }
  })

  .state('bmi', {
    url: "/bmi",

    views:{
      'bodyContent': {
        templateUrl: "templates/bmi.html",
        controller: "ReferencesController"
      }
    }

  })

  .state('gcs', {
    url: "/gcs",

    views:{
      'bodyContent': {
        templateUrl: "templates/gcs.html",
        controller: "ReferencesController"
      }
    }

  })

  .state('bodyfluids', {
    url: "/bodyfluids",

    views:{
      'bodyContent': {
        templateUrl: "templates/bodyfluids.html",
        controller: "ReferencesController"
      }
    }

  })

  .state('pregnancyWheel', {
    url: "/pregnancyWheel",

    views:{
      'bodyContent': {
        templateUrl: "templates/pregnancyWheel.html",
        controller: "ReferencesController"
      }
    }

  })

  .state('userCases', {
    url: "/userCases",
    cache: false,

    views: {
      'bodyContent' : {
        templateUrl: "templates/userCases.html",
        controller: "UserCaseController"
      }
    }
  })

  .state('editProfile', {
    url: "/editProfile",
    cache: false,

    views: {
      'bodyContent' : {
        templateUrl: "templates/editProfile.html",
        controller: "EditProfileController"
      }
    }
  })

  .state('list', {
    url: "/list",
    //parent: 'form',
    views: {
      'bodyContent' : {
        templateUrl: "templates/list.html",
        controller: "DoctorController"
      }
    }
  })

  .state('doctorDetails', {
    cache: false,
    url: "/doctorDetails",
    views: {
      'bodyContent' : {
        templateUrl: "templates/doctorDetails.html",
        controller: "DoctorDetailsController"
      }
    }
  })

  .state('claim', {
    url: "/claim",
    views: {
      'bodyContent' : {
        templateUrl: "templates/claim.html",
        controller: "DoctorController"
      }
    }
  })

  .state('login', {
    url: "/login",
    //parent: 'form',
    views: {
      'bodyContent' : {
        templateUrl: "templates/login.html",
        controller: "LoginController"
      }
    }
  })

  .state('signup', {
    url: "/signup",
    //parent: 'form',
    views: {
      'bodyContent' : {
        templateUrl: "templates/signup.html",
        controller: "LoginController"
      }
    }
  })

  .state('case', {
    url: "/case",
    cache: false,

    views: {
      'bodyContent' : {
        templateUrl: "templates/case.html",
        controller: "CaseController"
      }
    }
  })

  .state('caseDetail', {
    url: "/caseDetail",

    views: {
      'bodyContent' : {
        templateUrl: "templates/caseDetail.html",
        controller: "CaseController"
      }
    }
  })

  .state('journal', {
    url: "/journal",

    views: {
      'bodyContent' : {
        templateUrl: "templates/journal.html",
        controller: "JournalController"
      }
    }
  })

  .state('post', {
    url: "/post",

    views: {
      'bodyContent' : {
        templateUrl: "templates/post.html",
        controller: "PostController"
      }
    }
  })

  $urlRouterProvider.otherwise('/');
});
