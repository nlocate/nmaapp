var AD_DATA_URL = 'https://nma-app-ae317.firebaseio.com/ads.json'
// var AD_DATA_URL = 'http://localhost:3000/res'
var ADS_DATA = 'ADS_DATA'
nmaApp = angular.module('starter', ['ionic', 'ngCookies', 'ngFileUpload', 'ImgCache'])

nmaApp.factory('$localstorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}]);

nmaApp.config(function($httpProvider, ImgCacheProvider) {
  $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest' ;
  $httpProvider.defaults.withCredentials = true;
  // $httpProvider.defaults.headers.common['Access-Control-Allow-Credentials'] = 'true' ;
  // delete $httpProvider.defaults.headers.common['Access-Control-Request-Headers'];

  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

  // Override $http service's default transformRequest
  $httpProvider.defaults.transformRequest = [function(data) {
    return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
  }];

  var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

    for(name in obj) {
      value = obj[name];

      if(value instanceof Array) {
        for(i=0; i<value.length; ++i) {
          subValue = value[i];
          fullSubName = name + '[' + i + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value instanceof Object) {
        for(subName in value) {
          subValue = value[subName];
          fullSubName = name + '[' + subName + ']';
          innerObj = {};
          innerObj[fullSubName] = subValue;
          query += param(innerObj) + '&';
        }
      }
      else if(value !== undefined && value !== null)
        query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }

    return query.length ? query.substr(0, query.length - 1) : query;
  };

  ImgCacheProvider.setOptions({
    debug: false,
    usePersistentCache: true
  })
  ImgCacheProvider.manualInit = true;
})

nmaApp.run(function($timeout, $ionicPlatform, $http, $ionicPopup, $location, $cookies, $localstorage, $rootScope, ImgCache) {
  $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
  $rootScope.toggleModalFlag = $localstorage.get('modalDisplayer');

  $rootScope.apiBaseAddress = "http://nma.nlocate.com";

  $rootScope.isOffline = false;

  $rootScope.errorPopup = function(back) {
    var alertPopup = $ionicPopup.alert({
      title: 'Error',
      template: 'Sorry. There was an error communicating with the server'
    });
    alertPopup.then(function(res) {
      if (back) window.history.go(-1);
    });
  };

  $rootScope.alertPopup = function( title, message, callback ){
    $ionicPopup.alert({
      title: title,
      template: message 
    }).then(callback);
  }

  $rootScope.setModalDisplayer = function( visible ){
    if (visible){
      $localstorage.set('modalDisplayer', 'true');
    }else{
      $localstorage.set('modalDisplayer', 'false');
    }
    $timeout(function(){
      $rootScope.toggleModalFlag = $localstorage.get('modalDisplayer');
    }, 1000)
  }

  $rootScope.viewDetail = function(thisCase){
    $location.path('caseDetail');
    if (thisCase){
      $rootScope.caseId = thisCase.id;
    }

    var address = $rootScope.apiBaseAddress + "/case/view/" + $rootScope.caseId + "/?"+ Math.random();
    $http
    .get(address)
    .success(function(data, status_, headers, config){
      $rootScope.thisCase = data.case;
    });
  }

  $rootScope.isDoctor = $localstorage.get('loggedIn', false);

  $rootScope.isClaimed = $localstorage.get('claimed', false);

  if ( $localstorage.get('loggedIn') === 'true'){
    $rootScope.isLoggedIn = true;
  }

  var adsData = $localstorage.get(ADS_DATA)
  if (adsData) {
    $rootScope.adsData = JSON.parse(adsData);
    console.debug(JSON.parse(adsData), 'localstorage')
  }

  // $http.get(AD_DATA_URL)
  //   .success(function(data){
  //     console.log(AD_DATA_URL, data)
  //     $rootScope.adData = {ads: data};
  //     $localstorage.setObject(ADS_DATA, {ads: data});
  //   })
  //   .error(function(error){
  //     console.warn(error)
  //   })

  $http({
    method: 'GET',
    url: AD_DATA_URL,
    headers: {
      'Accept': 'application/json'
    },
    withCredentials: false
  }).then(
    function(res){
      console.log(AD_DATA_URL, res.data)
      $rootScope.adData = {ads: res.data};
      $localstorage.setObject(ADS_DATA, {ads: res.data});
    },
    function(error){
      console.log('ad fetch error', error)
    }
  )

  var references =
    [ 'bmi', 'bodyfluids', 'pregnancyWheel', 'gcs' ]

  $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams) {
      if ($rootScope.adsData){
        var thisAd =
          $rootScope.adsData.ads
          .filter(function(ad){
            return ad.name === toState.name
          })

        // if (!thisAd[0])
        //   console.log($rootScope.adsData.ads.filter(ad => ad.name === 'references_detail')[0])
        $rootScope.currentAd =
          thisAd.length > 0
          ? thisAd[0]
          : $rootScope.adsData.ads.filter(function(ad){
            return ad.name === 'references_detail'
          })[0]
      }
    }
  )

  $rootScope.goToAd = function(ad) {
    // window.open(ad.link_url, '_blank', 'location=yes,enableviewportscale=yes' );
    console.log(ad.link_url)
    window.open(ad.link_url, '_system')
  }

  $ionicPlatform.ready(function() {
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    function checkConnection(){
      $timeout(function(){
        $rootScope.isOffline = true;
      },0);

    }// end of checkConnection

    function goOnline(){
      $timeout(function(){
        $rootScope.isOffline = false;
      },0);
    }// end of checkConnection

    ImgCache.$init();

    // document.addEventListener("online", goOnline(), false);
    document.addEventListener("online", function(){
      $timeout(function(){
        $rootScope.isOffline = false;
      },0);
    }, false);
    document.addEventListener("offline", checkConnection, false);
  });
})
